var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0
var nEff = books.length
readBooks(10000, books[i], function (waktu1) { //books[0]
    if (i+1 < nEff && waktu1 >= books[i+1].timeSpent) {
        i += 1
        readBooks(waktu1, books[i], function (waktu2) {
            if (i+1 < nEff && waktu2 >= books[i+1].timeSpent) {
                i += 1
                readBooks(waktu2, books[i], function (waktu3) {
                    if (i+1 < nEff && waktu3 >= books[i+1].timeSpent) {
                        i += 1
                        readBooks(waktu3, books[i], function (waktu4){})
                    }
                    else {
                        return console.error();
                    }
                })
            }
            else {
                return console.error();
            }
        })
    }
    else {
        return console.error();
    }
})