var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var i = 0
var nEff = books.length
// new Promise( function (resolve, reject)
readBooksPromise(10000, books[i])
    .then(function (waktu) {
        i += 1
        readBooksPromise(waktu, books[i])
            .then(function (waktu) {
                i += 1
                readBooksPromise(waktu, books[i])
            })
            .catch(function (error) {
                return error;
            })
    })
    .catch(function (error) {
        return error;
    })
