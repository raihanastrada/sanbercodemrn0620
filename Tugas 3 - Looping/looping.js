// No. 1 Looping While
console.log("LOOPING PERTAMA")
var number1 = 2; // Assign nilai awal

while (number1 <= 20) {
    console.log(number1 + " - I love coding")
    number1 += 2
}
console.log("")

console.log("LOOPING KEDUA")
var number2 = 20; // Assign nilai awal

while (number2 > 0) {
    console.log(number2 + " - I will become a mobile developer")
    number2 -= 2    
}

// No. 2 Looping For
console.log("")
for (var i = 1; i <= 20; i++) {
    if (i%2 == 1) { // Kondisi jika bilangan ganjil
        if (i%3 == 0) { // Jika bilangan ganjil merupakan kelipatan 3
            console.log(i + " - I Love Coding")
        }
        else { // (i%3 != 0), Jika bilangan ganjil bukan kelipatan 3
            console.log(i + " - Santai")  
        }
    }
    else { // (i%2 == 0), Kondisi jika bilangan genap
        console.log(i + " - Berkualitas")
    }
}

// No. 3 Membuat Persegi Panjang
console.log("")
var cetak = ""
var baris = 4 // Assign baris
var kolom = 8 // Assign kolom

for (var j = 1; j <= kolom; j++) {
    cetak += "#" // Akan menambah '#' sebanyak kolom (8)
}
for (var i = 1; i <= baris; i++) {
    console.log(cetak) // Mencetak '########' sebanyak baris (4)
}

// No. 4 Membuat Tangga
console.log("")
var cetak = ""
var baris = 7 // Assign baris

for (var i = 1; i <= baris; i++) {
    cetak += '#' // Tanda '#' memiliki panjang sesuai baris
    console.log(cetak)
}

// No. 5 Membuat Papan Catur
console.log("")
var baris = 8

for (var i = 1; i <= baris; i++) {
    if (i%2 == 1) { // Kondisi jika baris ganjil
        console.log(" # # # #")
    }
    else { // (i%2 == 0), Kondisi jika baris genap
        console.log("# # # # ")
    }
}