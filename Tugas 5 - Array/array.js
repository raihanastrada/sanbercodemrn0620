// Soal No. 1 (Range)
function range(startNum, finishNum) {
    if (startNum == null || finishNum == null) {
        return -1
    }
    else { // startNum & finishNum memiliki nilai
        var array = []
        if (startNum <= finishNum) {
            while (startNum <= finishNum) {
                array.push(startNum)
                startNum += 1
            }
        }
        else {
            while (startNum >= finishNum) {
                array.push(startNum)
                startNum -= 1
            }
        }
        return array
    }
}

// Test Case
console.log("- No. 1 -")
console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

// Soal No. 2 (Range with Step)
function rangeWithStep (startNum, finishNum, step) {
    var array = []
    if (startNum <= finishNum) {
        while (startNum <= finishNum) {
            array.push(startNum)
            startNum += step
        }
    }
    else {
        while (startNum >= finishNum) {
            array.push(startNum)
            startNum -= step
        }
    }
    return array
}

// Test Case
console.log("")
console.log("- No. 2 -")
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step) {
    var sum = 0
    if (step == null) { // Jika step kosong, step -> 1
        if (startNum != null && finishNum == null) {
            return startNum
        }
        else {
            var tabNumber = rangeWithStep(startNum, finishNum, 1) // Membuat array dengan step = 1
        }
    }
    else { // Step tidak kosong
        var tabNumber = rangeWithStep(startNum, finishNum, step) // Membuat array dengan step ditentukan
    }

    for (var i = 0; i <= tabNumber.length-1; i++){ 
        sum += tabNumber[i] // Menjumlahkan semua elemen array
    }
    return sum
}

// Test Case
console.log("")
console.log("- No. 3 -")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array  Multidimensi)
function dataHandling(tabData) {
    for (var i = 0; i <= tabData.length-1; i++) {
        console.log("Nomor ID: "+ tabData[i][0])
        console.log("Nama Lengkap: "+ tabData[i][1])
        console.log("TTL: "+ tabData[i][2] +" "+tabData[i][3])
        console.log("Hobi: "+ tabData[i][4])
        console.log("")
    }
}
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log("")
console.log("- No. 4 -")
dataHandling(input)

// Soal No. 5 (Balik Kata)
function balikKata(string) {
    var stringInverse = ""
    for (var i = string.length-1; i >= 0; i--) {
        stringInverse += string[i] // Menambah huruf dari belakang
    }
    return stringInverse
}

console.log("- No. 5 -")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

// Soal No. 6 (Metode Array)
function dataHandling2 (array) {
    // Memodifikasi variabel tersebut agar menjadi seperti array yang ditentukan
    array.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(array)

    var tl = array[3].split("/") // Memisahkan antara tanggal, bulan, dan tahun
    var bulan = Number(tl[1])
    cekBulan(bulan) // Mengubah input bulan (int) menjadi nama bulan dengan fungsi cekBulan 
    
    var tl2 = [] // Inisiasi array baru yang akan disort descending
    for (var i=0; i < tl.length; i++) { // Mengubah type data dari setiap elemen tanggal lahir (string -> int)
        tl2[i] = Number(tl[i])
    }
    tl2.sort() // Menyortir data dengan nilai menurun
    for (var i=0; i < tl2.length; i++) { // Mengembalikan type data (int -> str)
        tl2[i] = String(tl2[i])
    }
    console.log(tl2)

    console.log(tl.join("-")) // Menggabungkan elemen array tl [tanggal,bulan,tahun]

    console.log(array[1].slice(0,15)) // Membatasi agar nama hanya terdiri dari 15 karakter
}

function cekBulan(num) { // Fungsi yang menerima masukan int kemudian mengembalikan nama bulan sesuai angka
    switch(num) {
        case 1:
            console.log("Januari");
            break;
        case 2:
            console.log("Fenruari");
            break;
        case 3:
            console.log("Maret");
            break;
        case 4:
            console.log("April");
            break;
        case 05:
            console.log("Mei");
            break;
        case 6:
            console.log("Juni");
            break;
        case 7:
            console.log("Juli");
            break;
        case 8:
            console.log("Agustus");
            break;
        case 9:
            console.log("September");
            break;
        case 10:
            console.log("Oktober");
            break;
        case 11:
            console.log("November");
            break;
        case 12:
            console.log("Desember");
            break;
    }
}
// Test Case
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("")
console.log("- No. 6 -")
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */
