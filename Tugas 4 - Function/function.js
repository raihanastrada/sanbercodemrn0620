// No. 1
console.log("No. 1")

//  Fungsi yang mengembalikan string "Halo Sanbers!"
function teriak() {
    return ("Halo Sanbers!")
}

console.log(teriak()) // "Halo Sanbers!"

// No. 2
console.log("")
console.log("No. 2")

// Fungsi yang menerima dua buah bilangan kemudian mengembalikan hasil kali kedua bilangan
function kalikan(a, b) { 
    return (a*b)
} 

var num1 = 12 // Assign number1
var num2 = 4 // Assign number2
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

// No. 3
console.log("")
console.log("No. 3")

// Fungsi yang menerima nama, umur, alamat, dan hobi kemudian mengembalikan kalimat perkenalan
function introduce(nama, umur, alamat, hobi) {
    return ("Nama saya "+nama+", umur saya "+umur+" tahun, alamat saya di "+alamat+", dan saya punya hobby yaitu "+hobi+"!")
}

var name = "Agus" // nama
var age = 30 // umur
var address = "Jln. Malioboro, Yogyakarta" // alamat
var hobby = "Gaming" // hobi
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"