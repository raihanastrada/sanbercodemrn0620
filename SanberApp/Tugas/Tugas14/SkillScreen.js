import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Data from './skillData.json';
import FlatItem from './flatitem.js';


export default class SkillScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                
                <Image source={require('./logo.png')}  style={styles.logo}/>    
            
                <View style={styles.profileBar}>
                    <Icon style={{color:'#3EC6FF'}} name='account-circle' size={35}/>    
                    <View style={styles.profileName}>
                        <Text>Hai,</Text>
                        <Text style={{color:'#003366', fontSize: 16}}>Raihan Astrada</Text>
                    </View>
                </View>

                <View style={styles.header}>
                    <Text style={{color:'#003366', fontSize: 36, marginTop: -10, fontWeight:'bold'}}>SKILL</Text>
                    <View style={styles.border} />
                </View>

                <View style={styles.navBar}>
                    <Text style={styles.tabBar}>Library / Framework</Text>
                    <Text style={styles.tabBar}>Bahasa Pemrograman</Text>
                    <Text style={styles.tabBar}>Teknologi</Text>
                </View>

                <View style={styles.body}>
                    <FlatList 
                    data={Data.items}
                    renderItem={(flat)=><FlatItem flat={flat.item} />}
                    keyExtractor={(item)=>item.id}
                    />
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  logo: {
    alignSelf: 'flex-end',
    width: 170,
    height: 170,
    resizeMode: 'contain',
    marginTop: -55
  },
  profileBar: {
    paddingLeft: 30,
    flexDirection: 'row',
    marginTop: -55  
  },
  profileName: {
    flexDirection: 'column',
    paddingLeft: 10
  },
  header: {
    paddingLeft: 30,
    paddingTop: 20
  },
  border: {
    backgroundColor: '#B4E9FF',
    paddingLeft: 30,
    width: 330,
    height: 5
  },
  navBar: {
    paddingTop : 10,
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  tabBar: {
      backgroundColor: '#B4E9FF',
      color: '#003366',
      fontSize: 12,
      borderRadius: 10,
      marginLeft: 10,
      padding: 5,
      fontWeight:'bold'
  },
  body: {
      flex: 1
  }
  
})

//lightBlue: '#3EC6FF'
//darkBlue: '#003366'
//grayBackground: '#EFEFEF'
//blueBackground: '#B4E9FF'