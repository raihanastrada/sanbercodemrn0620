import React from 'react';

// Tugas 1
import Main from './components/Main';
export default class App extends React.Component {
  render() {
    return (
      <Main />
    )
  }
}

// Tugas 2
import SkillScreen from './SkillScreen';
export default class App extends Component {
  render() {
    return (
      <SkillScreen />
    )
  }
}