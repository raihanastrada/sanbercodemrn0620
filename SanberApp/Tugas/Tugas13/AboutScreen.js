import React, { Component } from 'react'
import { Platform, View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
            <View style={styles.head}>
                <Text style={styles.title}>Tentang Saya</Text>
                <Icon style={styles.headProfile} name='account-circle' size={150}/>
                <Text style={styles.profileName}>Raihan Astrada F</Text>
                <Text style={styles.profileJob}>Software Engineer</Text>
            </View>

            <View style={styles.body}>
                <Text style={styles.barTitle}>Portofolio</Text>
                <View style={styles.boxTop}>
                    <TouchableOpacity>
                        <Icon2 style={styles.iconBar} name='gitlab' size={50}/>
                        <Text style={styles.iconTitle}>@raihanastrada</Text>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Icon2 style={styles.iconBar} name='github-circle' size={50} />
                        <Text style={styles.iconTitle}>@raihanastrada</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.bottom}>
                <Text style={styles.barTitle}>Hubungi Saya</Text>
                <View style={styles.boxBot}>
                    <TouchableOpacity style={styles.tabItemBot}>
                        <Icon2 style={styles.iconBar} name='facebook-box' size={40}/>
                        <Text style={styles.iconTitle}>Raihan Astrada</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItemBot}>
                        <Icon2 style={styles.iconBar} name='instagram' size={40}/>
                        <Text style={styles.iconTitle}>@raihanastrada</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItemBot}>
                        <Icon2 style={styles.iconBar} name='twitter' size={40}/>
                        <Text style={styles.iconTitle}>@raihanastrada</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>   
      )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    head: {
        paddingTop: 70,
        alignItems: 'center',
    },
    title: {
        color: '#003366',
        fontSize: 36,
    },
    headProfile: {
        color: '#EFEFEF',
        alignItems : 'center'
    },
    profileName: {
        color: '#003366',
        fontSize: 24,
    },
    profileJob: {
        color: '#3EC6FF',
        fontSize: 18
    },
    body: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20
    },
    barTitle: {
        borderBottomWidth: 1,
        borderColor: '#003366',
        backgroundColor: '#EFEFEF',
        flexDirection: 'row',
    },
    iconBar: {
        backgroundColor: '#EFEFEF',
        color: '#3EC6FF'
    },
    iconTitle: {
        fontSize: 14,
        color: '#003366',
        paddingTop: 7,
        paddingLeft: 10
    },
    bottom: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20
    },
    boxTop: {
        backgroundColor: '#EFEFEF',
        width: 335,
        height: 80,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    boxBot: {
        backgroundColor: '#EFEFEF',
        width: 335,
        height: 135,
        flexDirection: 'column',
        alignItems: 'center',
    },
    tabItemBot: {
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    }
})
//lightBlue: “#3EC6FF”
//darkBlue: “#003366”
//grayBackground: “#EFEFEF”