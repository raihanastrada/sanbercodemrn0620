import React, { Component } from 'react'
import { Platform, View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native'

export default class App extends Component {
    render() {
      return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('./images/logo.png')} style={{width:375, height:100}} />    
            </View>
            <View style={styles.body}>
                <Text style={styles.bodyTitle}>Login</Text>
            </View>
            <View style={styles.bodyBar}>
                <Text style={styles.bodySubtitle}>Username/Email</Text>
                <View style={styles.tabBar}/>
                <Text style={styles.bodySubtitle}>Password</Text>
                <View style={styles.tabBar}/>
            </View>
            <View style={styles.body}>
                <Text style={styles.loginButton}>Masuk</Text>
                <Text style={{color:'#3EC6FF', fontSize:18}}>atau</Text>
                <Text style={styles.registerButton}>Daftar ?</Text>     
            </View>
        </View>
      )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    logo: {
        alignItems: 'center',
        paddingTop: 80
    },
    body: {
        alignItems: 'center',
        paddingTop: 40,
    },
    bodyTitle: {
        fontSize: 20,
        alignItems: 'center',
        color: '#003366'
    },
    bodyBar: {
        paddingLeft: 50,
        paddingTop: 30,
    },
    bodySubtitle: {
        fontSize: 12,
        color: '#003366',
        paddingTop: 10,
    },
    tabBar: {
        backgroundColor: 'white',
        height: 40,
        width: 300,
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        borderColor: '#003366',
    },
    loginButton: {
        fontSize: 18,
        alignItems: 'center',
        color: 'white',
        height: 30,
        width: 100,
        backgroundColor: '#3EC6FF',
        paddingLeft: 25,
        paddingTop: 1
    },
    registerButton: {
        fontSize: 18,
        alignItems: 'center',
        color: 'white',
        height: 30,
        width: 100,
        backgroundColor: '#003366',
        paddingLeft: 20,
        paddingTop: 2
    },
})

//lightBlue: “#3EC6FF”
//darkBlue: “#003366”
//grayBackground: “#EFEFEF”