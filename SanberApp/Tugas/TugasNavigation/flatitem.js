import React, { Component } from 'react'
import { Platform, View, StyleSheet, Text, Image, TouchableOpacity, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class FlatItem extends Component {
    render(){
        let flat = this.props.flat
        return (
            <View style={styles.container}>
                <TouchableOpacity>
                <View style={styles.box}>
                    <Icon name={flat.iconName} size={80} color={'#003366'} alignSelf='center'/>
                    <View style={styles.description}>
                        <Text style={{paddingLeft: 10, fontSize: 20, fontWeight:'bold', color:'#003366'}}>{flat.skillName}</Text>
                        <Text style={{paddingLeft: 10, fontSize: 18, fontWeight:'bold', color:'#3EC6FF'}}>{flat.categoryName}</Text>
                        <Text style={{fontSize: 36, fontWeight:'bold', color:'white', alignSelf: 'flex-end'}}>{flat.percentageProgress}</Text>
                    </View>
                    <Icon name='chevron-right' size={80} color={'#003366'} alignSelf='center'/>
                </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      padding: 15,
    },
    box:{
        flexDirection: 'row',
        backgroundColor:'#B4E9FF',
        justifyContent: 'space-evenly',
        borderRadius: 20,
        borderBottomWidth: 5,
        borderBottomColor: '#EFEFEF'
    },
    description: {
        flexDirection: 'column',
        justifyContent: 'space-evenly'
    }
})

//lightBlue: '#3EC6FF'
//darkBlue: '#003366'
//grayBackground: '#EFEFEF'
//blueBackground: '#B4E9FF'