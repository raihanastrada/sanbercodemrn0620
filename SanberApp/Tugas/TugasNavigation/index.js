import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { LoginScreen } from './LoginScreen';
import { AboutScreen } from './AboutScreen';
import { SkillScreen } from './SkillScreen';
import { ProjectScreen } from './ProjectScreen';
import { AddScreen } from './AddScreen';

const Root = createStackNavigator();
const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

const TabsScreen = () => (
    <Tabs.Navigator>
      <Tabs.Screen name='Skill Screen' 
      component={SkillScreen} />
      <Tabs.Screen name='Project Screen'
      component={ProjectScreen} />
      <Tabs.Screen name ='Add Screen'
      component={AddScreen} />
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name='Login Screen' component={LoginScreen} />
        <Drawer.Screen name='Tab Screen' component={TabsScreen} />
        <Drawer.Screen name='About Screen' component={AboutScreen} />
    </Drawer.Navigator>
)

export default function App() {
    return (
        <NavigationContainer>
            <Root.Navigator>
                <Root.Screen name='Root Screen' component={DrawerScreen} />
            </Root.Navigator>
        </NavigationContainer>
    )
}

// Stacknavigator : LoginScreen & DrawerScreen
// DrawerNavigator : TabScreen & AboutScreen
// TabNavigator : SkillScreen, ProjectScreen, AddScreen