// Soal No. 1 (Array to Object)
console.log("- No. 1 -")
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var person = {}
    if (arr.length == 0) {
        console.log("")
    }
    else { // (arr != null)
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][3] > thisYear || arr[i][3] == null) {
                var age = "Invalid Birth Year"
            }
            else {
                var age = thisYear - arr[i][3]
            } 
            person.firstName = arr[i][0]
            person.lastName = arr[i][1]
            person.gender = arr[i][2]
            person.age = age
            
            
            console.log([i+1]+". "+person.firstName+" "+person.lastName+":")
            console.log(person)
        } 
    }
      
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

// Soal No. 2 (Shopping Time)
console.log("- No. 2 -")
function shoppingTime(memberId, money) {
    if (memberId == null || memberId =="") {
        return("Mohon maaf, toko X hanya berlaku untuk member saja")
    }
    else if (money < 50000) {
        return("Mohon maaf, uang tidak cukup")
    }
    else {
        var object = {}
        object.memberId = memberId
        object.money = money
        listPurchased = []
        
        var Stacattu = false
        var Zoro = false
        var HnN = false
        var Uniklooh = false
        var Casing = false

        for (var i = 1; i <= 5 ; i++) {
            if (money >= 1500000 && Stacattu == false) {
                money -= 1500000
                listPurchased.push("Sepatu Stacattu")
                Stacattu = true
            }
            else if(money >= 500000 && Zoro == false) {
                money -= 500000
                listPurchased.push("Baju Zoro")
                Zoro = true
            }
            else if(money >= 250000 && HnN == false) {
                money -= 250000
                listPurchased.push("Baju H&N")
                HnN = true
            }
            else if(money >= 175000 && Uniklooh == false) {
                money -= 175000
                listPurchased.push("Sweater Uniklooh")
                Uniklooh = true
            }
            else if(money >= 50000 && Casing == false) {
                money -= 50000
                listPurchased.push("Casing Handphone")
                Casing = true
            }
        }
        object.listPurchased = listPurchased
        object.changeMoney = money
        return (object)
    }
}
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if (arrPenumpang.length == 0) {
        return []
    }
    else {
        var orang = {}
        var arrayHasil = []
        temp = {}
        for (var i = 0; i < arrPenumpang.length; i++) {
            titikAwal = nilaiTitik(arrPenumpang[i][1])
            titikAkhir = nilaiTitik(arrPenumpang[i][2])
            
            penumpang = {
                penumpang : arrPenumpang[i][0],
                naikDari : arrPenumpang[i][1],
                tujuan : arrPenumpang[i][2],
                bayar : (titikAkhir-titikAwal)*2000
            }
            arrayHasil.push(penumpang)
        }
        return arrayHasil

    }
}
function nilaiTitik(titik) {
    switch(titik) {
        case ("A"):
            return 1
        case ("B"):
            return 2
        case ("C"):
            return 3
        case ("D"):
            return 4
        case ("E"):
            return 5
        case ("F"):
            return 6
    }
}

  //TEST CASE
  console.log("- No. 3 -")
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]  